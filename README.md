
## Project creation

```sh
mkdir django-users	# directorio del projecto
cd django-users
```
```sh
pipenv install		# para crear un entorno virtual en el dir actual
pipenv shell		# para activar el entorno (debe cambiar el prompt)
```
Esto nos genera los siguientes archivos:
```sh
.
├── Pipfile		# como el Gemfile y Gemfile.lock de rails
└── Pipfile.lock
```
```sh
pipenv install django	# instala la ultima version de django
```
Usar pipenv para instalar lo que sea necesario, no pip! (de esta forma las dependencias quedaran registradas en nuestro Pipfile)

En los Pipfiles ahora aparece django
```sh
djanjo-admin startproject users_project .	# para crear un projecto django
```
El punto final es para crearlo en el directorio actual
El nombre no importa, solo es un contenedor del projecto y se puede cambiar

```sh
.
├── db.sqlite3
├── manage.py			# cli para interactuar con django
├── users_project		# contenedor para la conf del proyecto
│   ├── __init__.py		# le dice a python que el directorio es un paquete (ver paquetes pyton)
│   ├── settings.py		# archivo de conf del proyecto
│   ├── urls.py			# archivo de rutas del proyecto
│   └── wsgi.py			# web server gateway interface (para producción)
├── Pipfile
└── Pipfile.lock
```
```sh
python manage.py runserver	# corre el servidor local (... runserver 0:8000 para cambiar host:port)
```

## User application

Creamos la aplicación usuario
```sh
python manage.py startapp users
```
https://docs.djangoproject.com/en/2.2/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project

Ahí Django recomienda que siempre que se comienze un proyecto
se cree un custom user model, aunque el que venga por defecto nos alcance.
Este nuevo custom user es idéntico al que viene por defecto pero tendremos
la posibilidad de modificarlo si es necesario

Django facilita el uso de modelos porque por detras usa managers para esos modelos
y para nuestro modelo User tendremos que crear un manager también, esto
se hace facilmente pues extedemos del modelo y manager por defecto

Por lo que nuestro users/models.py quedaria asi:

```python
from django.db import models

from django.contrib.auth.models import AbstractUser, UserManager


class CustomUserManager(UserManager):
    pass


class CustomUser(AbstractUser):
    objects = CustomUserManager()
```

En setting.py se registra la app users en settings.INSTALLED_APPS
y ahi mismo al final le indicamos a django que no use el modelo user
por defecto, sino que use el nuestro

```python
	...
	'users',
]

AUTH_USER_MODEL =  'users.CustomUser'
```

como esto hay que hacerlo antes de correr el primer migrate borro todos los datos
```sh
python manage.py sqlflush | python manage.py dbshell
```

Para usar un custom user también necesitamos personalizar los forms por defecto
UserCreationForm y UserChangeForm, esto lo hacemos en users/forms.py

```python
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = UserCreationForm.Meta.fields # + ('custom_field',)


class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm):
        model = CustomUser
        fields = UserChangeForm.Meta.fields # + ('custom_field',)
```

https://docs.djangoproject.com/en/2.2/topics/auth/customizing/#custom-users-and-the-built-in-auth-forms

Por último hay que modificar el admin pues esta altamente acoplado con el user por defecto
lo hacemos en admin.py

```python
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    model = CustomUser
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm


admin.site.register(CustomUser, CustomUserAdmin)
```

y con esto tenemos nuestro custom user creado

Recién ahora podemos crear y correr las migraciones

```sh
python manage.py makemigrations
python manage.py migrate

python manage.py createsuperuser
puthon manage.py runserver
```
Si visitamos http://localhost:8000/admin  y podemos ingresar con éxito, entonces todo funcionó pues aquí ya estamos usando nuestro custom user